// ==UserScript==
// @name         Ethermine-converter
// @namespace    https://gitlab.com/BlackRockSoul/
// @version      0.1
// @description  Convert ETH to USD and RUB
// @author       BlackRockSoul
// @match        https://ethermine.org/miners/*
// @grant        none
// @require      https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js
// ==/UserScript==

(function() {
    'use strict';

    var eth_price_block = $('.panel-info > .panel-body > h4'),
        eth_price = parseFloat(eth_price_block.text());


    $.getJSON('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD,RUB', function(response){
        console.log(response.USD, response.RUB);

        var eth_usd = "$" + (response.USD * eth_price).toFixed(2);
        var eth_rub = "₽" + (response.RUB * eth_price).toFixed(1);

        eth_price_block.after("<p>" + eth_usd + "<br>" + eth_rub + "</p>");

    });

})();